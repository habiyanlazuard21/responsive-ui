import 'package:flutter/material.dart';

import '../../../../lib.dart';

class SponsorPages extends StatefulWidget {
  const SponsorPages({super.key});

  @override
  State<SponsorPages> createState() => _SponsorPagesState();
}

class _SponsorPagesState extends State<SponsorPages> {
  bool _isTablet = false;
  bool _isMobile = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _isTablet = ResponsiveUtils(context).getMediaQueryWidth() <= 948;
        _isMobile = ResponsiveUtils(context).getMediaQueryWidth() < 480;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: AppColors.green,
      width: double.infinity,
      height: 70,
      padding: _isMobile
          ? const EdgeInsets.symmetric(
              vertical: AppGap.tiny,
              horizontal: AppGap.medium,
            )
          : const EdgeInsets.symmetric(
              vertical: AppGap.tiny,
              horizontal: AppGap.giant,
            ),
      child: Wrap(
        spacing: 27.0,
        runSpacing: 27.0,
        alignment: WrapAlignment.spaceBetween,
        children: [
          // ...List.generate(
          //   _getSponsorList.length,
          //   (index) => CustomImageWrapper(
          //     image: _getSponsorList[index].sponsor,
          //     width: 125,
          //     height: 42,
          //     fit: BoxFit.fitWidth,
          //     isNetworkImage: false,
          //   ),
          // ),
        ],
      ),
    );
  }
}
