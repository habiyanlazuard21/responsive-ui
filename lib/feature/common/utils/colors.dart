import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF111111);
  static const Color main = Color(0xFFA67C52);
  static const Color secondary = Color(0xFFF85E9F);
  static const Color red = Color(0xFFFF595C);
  static const Color black60 = Color(0xFFA3A3A3);
  static const Color green = Color(0xFF18BC54);
  static const Color grey = Color(0xFFB4B4B4);
  static const Color transparent = Colors.transparent;
  static const Color test = Color(0xFFEBE5DE);
  static const Color texthintgrey = Color(0xff6D6D6D);
  static const Color primary = Color(0xFF5D50C6);
  static const Color orange = Color(0xffFF5722);

  static const Color accentColor = Color(0xFF96B8B6);
  static const Color greyShade = Color(0xFFF1F1F1);
  static const Color blackShade = Color(0xFF555555);

  static const Color errorColor = Color(0xffd91717);
  static const Color errorColorTransparent = Color(0xffefd7d7);
  static const Color successColor = Color(0xff16b826);
  static const Color successColorTransparent = Color(0xfff3fff0);

  static const Color greyTracking = Color(0xffd9d9d9);
  static const Color textBlack = Color(0xff6d6d6d);
  static const Color textGrey = Color(0xffDADADA);
  static const Color textWafer = Color(0xffE1D3C9);
  static const Color textSilver = Color(0xffA5A3A3);
  static const Color colorTextField = Color(0xffEBE5DE);
  static const Color greyeScale = Color(0xff191825);
  static const Color yellow = Color(0xffFACD49);
  static const Color secondaryLight = Color.fromRGBO(250, 205, 73, 0.08);
}

class TextFieldColors {
  static const Color backgroundEnable = AppColors.greyShade;
  static const Color backgroundDisable = AppColors.blackShade;
  static const Color text = AppColors.black;
  static const Color hint = AppColors.black60;
  static const Color label = AppColors.black;
  static const Color errorBorder = AppColors.red;
  static const Color enabledBorder = AppColors.white;
  static const Color focusedBorder = AppColors.white;
}
