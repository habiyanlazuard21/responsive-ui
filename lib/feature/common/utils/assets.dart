class AppIcon {
  static const _baseIconAssets = './assets/icons';

  static const logo = '$_baseIconAssets/ic_travelog.png';
  static const bag = '$_baseIconAssets/ic_bag.png';
  static const burgerIcon = '$_baseIconAssets/ic_burger_icon.png';
  static const booking = '$_baseIconAssets/ic_booking.png';
  static const colud = '$_baseIconAssets/ic_cloudy.png';
  static const destination = '$_baseIconAssets/ic_destination.png';
  static const location = '$_baseIconAssets/ic_location.png';
  static const calender = '$_baseIconAssets/ic_calender.png';
  static const ticket = '$_baseIconAssets/ic_ticket.png';
  static const facebook = '$_baseIconAssets/ic_facebook.png';
  static const instagram = '$_baseIconAssets/ic_instagram.png';
  static const twitter = '$_baseIconAssets/ic_twitter.png';
}

class AppIllustrations {
  static const _baseIllustrationAssets = './assets/illustrations';

  static const backgroundSide = '$_baseIllustrationAssets/ill_objects_side.png';
  static const airbnb = '$_baseIllustrationAssets/ill_airbnb.png';
  static const booking = '$_baseIllustrationAssets/ill_booking.png';
  static const expedia = '$_baseIllustrationAssets/ill_expedia.png';
  static const rbits = '$_baseIllustrationAssets/ill_rbits.png';
  static const tripadvisor = '$_baseIllustrationAssets/ill_tripadvisor.png';
}

class AppImages {
  static const _baseImagesAssets = './assets/images';

  static const serviceMobile = '$_baseImagesAssets/service_mobile.png';
  static const serviceWeb = '$_baseImagesAssets/service_web.png';
  static const travelMobile = '$_baseImagesAssets/travel_mobile.png';
  static const travelWeb = '$_baseImagesAssets/travel_web.png';
  static const travelPointMobile = '$_baseImagesAssets/travelpoint_mobile.png';
  static const travelPointWeb = '$_baseImagesAssets/travelpoint_web.png';
  static const exploreTopDestination =
      '$_baseImagesAssets/explore_top_destination.png';

  static const fish = '$_baseImagesAssets/fish.png';
  static const montain = '$_baseImagesAssets/montain.png';
  static const beach = '$_baseImagesAssets/explore_top_destination.png';
  static const keyFeatures = '$_baseImagesAssets/key_features.png';
  static const testimonials = '$_baseImagesAssets/testimonials.png';
  static const bgTestimonial = '$_baseImagesAssets/baground_testimonial.png';
  static const bgGraphic = '$_baseImagesAssets/Graphic_Elements.png';
  static const bgSideRight = '$_baseImagesAssets/bg_object_right.png';
}
