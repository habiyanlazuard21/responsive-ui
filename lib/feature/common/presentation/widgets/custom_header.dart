import 'package:flutter/material.dart';

import '../../../../lib.dart';

class CustomHeader extends StatelessWidget {
  final String _title;
  final Function() _onPressedBack;
  final Widget? _suffix;
  final List<Widget>? _children;
  final Widget? _headerPinned;
  final Widget? _child;
  const CustomHeader({
    Key? key,
    required String title,
    required Function() onPressedBack,
    Widget? suffix,
    List<Widget>? children,
    Widget? headerPinned,
    Widget? child,
  })  : _onPressedBack = onPressedBack,
        _title = title,
        _suffix = suffix,
        _children = children,
        _headerPinned = headerPinned,
        _child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      onWillPop: _onPressedBack,
      colorContainer: const Color(0xFFF4F0ED),
      child: LayoutBuilder(builder: (context, constraint) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraint.maxHeight),
          child: IntrinsicHeight(
            child: Column(
              children: [
                /// Back Arrow
                Padding(
                  padding: const EdgeInsets.only(
                    left: AppGap.medium,
                    top: 10,
                    right: AppGap.medium,
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: _onPressedBack,
                        tooltip: "Back",
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          size: AppIconSize.normal,
                          color: Colors.black,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          _title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.semiBold
                              .copyWith(fontSize: AppFontSize.large),
                        ),
                      ),
                      _suffix ?? const SizedBox(),
                    ],
                  ),
                ),

                /// Here is The Body
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(AppBorderRadius.extraLarge),
                      ),
                    ),
                    child: Column(
                      children: [
                        _headerPinned ?? const SizedBox(),
                        if (_child != null) ...[
                          Expanded(child: _child!)
                        ] else ...[
                          Expanded(
                            child: CustomSingleChildScrollViewWrapper(
                              child: ColumnPadding(
                                padding: EdgeInsets.only(
                                  left: AppGap.extraLarge,
                                  right: AppGap.extraLarge,
                                  top: AppGap.normal,
                                  bottom: ResponsiveUtils(context)
                                      .getResponsiveBottomPadding(),
                                ),
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: _children ?? [],
                              ),
                            ),
                          )
                        ],
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class CustomSliverHeader extends StatelessWidget {
  final String _title;
  final Function() _onPressedBack;
  final Widget? _suffix;
  final List<Widget> _slivers;
  final Future<void> Function()? _onRefresh;
  const CustomSliverHeader({
    Key? key,
    required String title,
    required Function() onPressedBack,
    Widget? suffix,
    required List<Widget> slivers,
    Future<void> Function()? onRefresh,
  })  : _onPressedBack = onPressedBack,
        _title = title,
        _suffix = suffix,
        _slivers = slivers,
        _onRefresh = onRefresh,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      onWillPop: _onPressedBack,
      colorContainer: const Color(0xFFF4F0ED),
      child: LayoutBuilder(builder: (context, constraint) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraint.maxHeight),
          child: IntrinsicHeight(
            child: Column(
              children: [
                /// Back Arrow
                Padding(
                  padding: const EdgeInsets.only(
                    left: AppGap.medium,
                    top: 10,
                    right: AppGap.medium,
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: _onPressedBack,
                        tooltip: "Back",
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          size: AppIconSize.normal,
                          color: Colors.black,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          _title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.bold
                              .copyWith(fontSize: AppFontSize.large),
                        ),
                      ),
                      _suffix ?? const SizedBox(),
                    ],
                  ),
                ),

                /// Here is The Body
                Expanded(
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(AppBorderRadius.extraLarge),
                      ),
                    ),
                    child: Visibility(
                      visible: _onRefresh != null,
                      replacement: CustomScrollViewWrapper(slivers: _slivers),
                      child: RefreshIndicator(
                        onRefresh: _onRefresh ?? () async {},
                        child: CustomScrollViewWrapper(slivers: _slivers),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class CustomHeaderWithLogo extends StatelessWidget {
  final Widget _child;
  const CustomHeaderWithLogo({
    Key? key,
    required Widget child,
  })  : _child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      onWillPop: () => Navigator.pop(context),
      colorContainer: const Color(0xFFF4F0ED),
      child: LayoutBuilder(builder: (context, constraint) {
        return CustomSingleChildScrollViewWrapper(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Stack(
                children: [
                  /// Back Arrow
                  Padding(
                    padding: const EdgeInsets.only(left: 24, top: 20),
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        Icons.arrow_back_ios,
                        size: AppIconSize.normal,
                        color: Colors.black,
                      ),
                    ),
                  ),

                  /// Body
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 90),
                    padding: const EdgeInsets.only(top: 80),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(AppBorderRadius.extraLarge),
                      ),
                    ),
                    child: _child,
                  ),

                  /// Icon Logo
                  Align(
                    alignment: Alignment.topCenter,
                    child: Image.asset(
                      AppIcon.logo,
                      width: 80,
                      height: 80,
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
