import 'package:flutter/material.dart';

import '../../../../lib.dart';

class ButtonPrimary extends StatelessWidget {
  const ButtonPrimary(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    String image = '',
    TextDecoration? textDecoration,
    Color foregroundbutton = Colors.transparent,
    double? elevation,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _textDecoration = textDecoration,
        _foregroundButton = foregroundbutton,
        _elevation = elevation,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final TextDecoration? _textDecoration;
  final Color? _foregroundButton;
  final double? _elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: _elevation,
          backgroundColor: _buttonColor,
          foregroundColor: _foregroundButton,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Text(
          _label,
          style: TextStyle(
            fontSize: _fontSize,
            color: _labelColor,
            fontWeight: _fontWeight,
            decoration: _textDecoration,
          ),
          textAlign: _textAlign,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}

class ButtonPrimaryContinue extends StatelessWidget {
  const ButtonPrimaryContinue(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: Icon(
                Icons.arrow_forward_ios,
                size: AppIconSize.normal,
                color: _labelColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ButtonPrimaryIconLeft extends StatelessWidget {
  const ButtonPrimaryIconLeft(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    Color iconColor = AppColors.main,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double iconSize = AppIconSize.tiny,
    double? width,
    IconData? icon,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _icon = icon,
        _iconColor = iconColor,
        _iconSize = iconSize,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final IconData? _icon;
  final Color _iconColor;
  final double _iconSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: Icon(
                _icon,
                size: _iconSize,
                color: _iconColor,
              ),
            ),
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}

class ButtonPrimaryIconRight extends StatelessWidget {
  const ButtonPrimaryIconRight(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    Color iconColor = AppColors.main,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    IconData? icon,
    double imageSize = AppIconSize.tiny,
    BoxFit fit = BoxFit.cover,
    bool isIconDisabled = false,
    String image = AppIcon.bag,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _icon = icon,
        _iconColor = iconColor,
        _isIconDisabled = isIconDisabled,
        _image = image,
        _imageSize = imageSize,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final IconData? _icon;
  final Color _iconColor;
  final bool _isIconDisabled;
  final String _image;
  final double _imageSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            const Gap(width: AppGap.normal),
            Container(
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: AppGap.tiny),
              child: _isIconDisabled
                  ? Icon(
                      _icon,
                      size: AppIconSize.normal,
                      color: _iconColor,
                    )
                  : Image.asset(
                      _image,
                      width: _imageSize,
                      height: _imageSize,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
