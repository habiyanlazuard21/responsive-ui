import 'package:responsive_ui/feature/common/common.dart';

class DestinationImageEntity {
  String images;
  String title;
  String subtitle;
  String rating;
  String price;

  DestinationImageEntity({
    this.images = '',
    this.title = '',
    this.subtitle = '',
    this.rating = '',
    this.price = '',
  });

  static List<DestinationImageEntity> getListExploreDestination = [
    DestinationImageEntity(
      images: AppImages.beach,
      title: "Paradise beach, Bantayan Island",
      subtitle: "Rome, Itally",
      rating: "4.8",
      price: "\$ 550.16",
    ),
    DestinationImageEntity(
      images: AppImages.fish,
      title: "Ocean with  full of Colors",
      subtitle: "Meldives",
      rating: "4.5",
      price: "\$ 20.99",
    ),
    DestinationImageEntity(
      images: AppImages.montain,
      title: "Mountain View, Above the cloud",
      subtitle: "United Emirated Arab",
      rating: "5.0",
      price: "\$ 150.99",
    ),
    DestinationImageEntity(
      images: AppImages.beach,
      title: "Paradise beach, Bantayan Island",
      subtitle: "Rome, Itally",
      rating: "4.8",
      price: "\$ 550.16",
    ),
    DestinationImageEntity(
      images: AppImages.fish,
      title: "Ocean with  full of Colors",
      subtitle: "Meldives",
      rating: "4.5",
      price: "\$ 20.99",
    ),
    DestinationImageEntity(
      images: AppImages.montain,
      title: "Mountain View, Above the cloud",
      subtitle: "United Emirated Arab",
      rating: "5.0",
      price: "\$ 150.99",
    ),
    DestinationImageEntity(
      images: AppImages.beach,
      title: "Paradise beach, Bantayan Island",
      subtitle: "Rome, Itally",
      rating: "4.8",
      price: "\$ 550.16",
    ),
    DestinationImageEntity(
      images: AppImages.fish,
      title: "Ocean with  full of Colors",
      subtitle: "Meldives",
      rating: "4.5",
      price: "\$ 20.99",
    ),
    DestinationImageEntity(
      images: AppImages.montain,
      title: "Mountain View, Above the cloud",
      subtitle: "United Emirated Arab",
      rating: "5.0",
      price: "\$ 150.99",
    ),
  ];
}
