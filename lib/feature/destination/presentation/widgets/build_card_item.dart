import 'package:flutter/material.dart';

import '../../../../lib.dart';

class BuildCardItemMobile extends StatelessWidget {
  const BuildCardItemMobile({
    double width = 373,
    required String image,
    required String title,
    required String subtitle,
    required String price,
    double height = 555,
    String rating = "4.8",
    Key? key,
  })  : _titile = title,
        _image = image,
        _width = width,
        _height = height,
        _subtitle = subtitle,
        _price = price,
        _rating = rating,
        super(key: key);

  final String _image;
  final String _titile;
  final String _subtitle;
  final String _price;
  final String _rating;
  final double _width;
  final double _height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _width,
      height: _height,
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(AppBorderRadius.big),
          border: Border.all(color: AppColors.greyShade, width: 1),
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, 0),
              blurRadius: 0,
              spreadRadius: 0,
              color: Color(0x0D000000),
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(AppBorderRadius.big),
              topRight: Radius.circular(AppBorderRadius.big),
            ),
            child: CustomImageWrapper(
              image: _image,
              width: double.infinity,
              height: 310,
              isNetworkImage: false,
            ),
          ),
          const Gap(height: AppGap.medium),
          ColumnPadding(
            padding: EdgeInsets.all(
              ResponsiveUtils(context).getResponsivePadding(AppGap.normal,
                  desktop: AppGap.big + AppGap.tiny),
            ),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Gap(height: AppGap.medium),
              Text(
                _price,
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.secondary,
                  fontSize: AppFontSize.big,
                ),
              ),
              const Gap(height: AppGap.small),
              Text(
                _titile,
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              const Gap(height: AppGap.medium),
              Text(
                _subtitle,
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.large,
                ),
              ),
              const Gap(height: AppGap.medium),
              RowPadding(children: [
                Text(
                  _rating,
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.orange,
                    fontSize: AppFontSize.big,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.star,
                    size: AppIconSize.normal,
                    color: AppColors.orange,
                  ),
                )
              ]),
            ],
          ),
        ],
      ),
    );
  }
}

class BuildCardItemWeb extends StatelessWidget {
  const BuildCardItemWeb({
    double width = 373,
    required String image,
    required String title,
    required String subtitle,
    required String price,
    double height = 575,
    String rating = "4.8",
    Key? key,
  })  : _titile = title,
        _image = image,
        _width = width,
        _height = height,
        _subtitle = subtitle,
        _price = price,
        _rating = rating,
        super(key: key);

  final String _image;
  final String _titile;
  final String _subtitle;
  final String _price;
  final String _rating;
  final double _width;
  final double _height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _width,
      height: _height,
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(AppBorderRadius.big),
          border: Border.all(color: AppColors.greyShade, width: 1),
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, 0),
              blurRadius: 0,
              spreadRadius: 0,
              color: Color(0x0D000000),
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(AppBorderRadius.big),
              topRight: Radius.circular(AppBorderRadius.big),
            ),
            child: CustomImageWrapper(
              image: _image,
              width: double.infinity,
              height:
                  ResponsiveUtils(context).getResponsiveSize(160, desktop: 310),
              isNetworkImage: false,
            ),
          ),
          const Gap(height: AppGap.medium),
          ColumnPadding(
            padding: EdgeInsets.all(
              ResponsiveUtils(context)
                  .getResponsivePadding(AppGap.normal, desktop: AppGap.big),
            ),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: ResponsiveUtils(context)
                        .getResponsiveSize(110, desktop: 180),
                    child: Text(
                      _titile,
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.black,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.normal,
                          desktop: AppFontSize.extraLarge,
                        ),
                      ),
                    ),
                  ),
                  const Gap(height: AppGap.small),
                  Text(
                    _price,
                    style: AppTextStyle.bold.copyWith(
                      color: AppColors.secondary,
                      fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                        AppFontSize.small,
                        desktop: AppFontSize.extraLarge,
                      ),
                    ),
                  ),
                ],
              ),
              const Gap(height: AppGap.small),
              Text(
                _subtitle,
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.black,
                  fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                    AppFontSize.normal,
                    desktop: AppFontSize.large,
                  ),
                ),
              ),
              const Gap(height: AppGap.medium),
              RowPadding(children: [
                Text(
                  _rating,
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.orange,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                      AppFontSize.normal,
                      desktop: AppFontSize.extraLarge,
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.star,
                    size: ResponsiveUtils(context).getResponsiveIconSize(
                      AppIconSize.normal,
                      desktop: AppIconSize.large,
                    ),
                    color: AppColors.orange,
                  ),
                )
              ]),
            ],
          ),
        ],
      ),
    );
  }
}
