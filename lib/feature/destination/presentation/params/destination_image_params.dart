class DestinationImageParams {
  String images;
  String title;
  String subtitle;
  String rating;
  String price;

  DestinationImageParams({
    this.images = '',
    this.title = '',
    this.subtitle = '',
    this.rating = '',
    this.price = '',
  });
}
