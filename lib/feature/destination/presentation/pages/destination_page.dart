import 'package:flutter/material.dart';
import 'package:responsive_ui/feature/destination/data/entities/detination_image_entity.dart';

import '../../../../lib.dart';

class DestinationPages extends StatefulWidget {
  const DestinationPages({super.key});

  @override
  State<DestinationPages> createState() => _DestinationPagesState();
}

class _DestinationPagesState extends State<DestinationPages> {
  int _indexedPages = 0;
  late PageController _pageController;
  late PageController _pageControllerWeb;

  final List<DestinationImageEntity> _destinationImageEntity =
      DestinationImageEntity.getListExploreDestination;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _indexedPages);
  }

  @override
  void dispose() {
    _pageControllerWeb.dispose();
    super.dispose();
  }

  void _nextPages() {
    if (_indexedPages < (_destinationImageEntity.length / 3).ceil() - 1) {
      _pageController.nextPage(
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    }
  }

  void _prevPages() {
    if (_indexedPages > 0) {
      _pageController.previousPage(
        duration: const Duration(milliseconds: 300),
        curve: Curves.bounceInOut,
      );
    }
  }

  void _onChanged(int next) {
    setState(() {
      _indexedPages = next;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<DestinationImageEntity> _getExplorerItemList =
        DestinationImageEntity.getListExploreDestination;

    return LayoutBuilder(
      builder: (ctx, constraints) {
        if (constraints.maxWidth > 600) {
          return _DestinationPageWeb(
            itemExplorerImage: _getExplorerItemList,
            controller: _pageController,
            nextPages: _nextPages,
            onChanged: _onChanged,
            prevPages: _prevPages,
          );
        } else {
          return _DestinationPageMobile(
            itemExplorerImage: _getExplorerItemList,
          );
        }
      },
    );
  }
}

class _DestinationPageMobile extends StatefulWidget {
  const _DestinationPageMobile({
    Key? key,
    required List<DestinationImageEntity> itemExplorerImage,
  })  : _itemExplorerImage = itemExplorerImage,
        super(key: key);

  final List<DestinationImageEntity> _itemExplorerImage;

  @override
  State<_DestinationPageMobile> createState() => _DestinationPageMobileState();
}

class _DestinationPageMobileState extends State<_DestinationPageMobile> {
  int _indexedPages = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _indexedPages);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      padding: const EdgeInsets.symmetric(horizontal: AppGap.medium),
      child: Column(
        children: [
          Text(
            "TOP DESTINATION",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.secondary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.medium),
          Text(
            "Explore Top Destination",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.black,
              fontSize: AppFontSize.extraBig,
            ),
          ),
          const Gap(height: AppGap.medium),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 64,
                height: 64,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.greyShade, width: 1),
                  shape: BoxShape.circle,
                  color: AppColors.white,
                ),
                child: IconButton(
                  onPressed: () {
                    if (_indexedPages > 0) {
                      _pageController.previousPage(
                        duration: const Duration(microseconds: 300),
                        curve: Curves.bounceInOut,
                      );
                    }
                  },
                  icon: const Icon(
                    Icons.arrow_back_rounded,
                    size: AppIconSize.normal,
                  ),
                ),
              ),
              Container(
                width: 64,
                height: 64,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.greyShade, width: 1),
                  shape: BoxShape.circle,
                  color: AppColors.primary,
                ),
                child: IconButton(
                  onPressed: () {
                    if (_indexedPages < widget._itemExplorerImage.length - 1) {
                      _pageController.nextPage(
                        duration: const Duration(microseconds: 300),
                        curve: Curves.easeInOut,
                      );
                    }
                  },
                  icon: const Icon(
                    Icons.arrow_forward,
                    color: AppColors.white,
                    size: AppIconSize.normal,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.medium),
          SizedBox(
            width: ResponsiveUtils(context).getMediaQueryWidth(),
            height: 594,
            child: PageView.builder(
              controller: _pageController,
              itemCount: widget._itemExplorerImage.length,
              physics: const NeverScrollableScrollPhysics(),
              onPageChanged: (pages) {
                setState(() {
                  _indexedPages = pages;
                });
              },
              itemBuilder: (context, index) {
                return BuildCardItemMobile(
                  width: double.infinity,
                  image: widget._itemExplorerImage[index].images,
                  title: widget._itemExplorerImage[index].title,
                  subtitle: widget._itemExplorerImage[index].subtitle,
                  price: widget._itemExplorerImage[index].price,
                  rating: widget._itemExplorerImage[index].rating,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class _DestinationPageWeb extends StatelessWidget {
  const _DestinationPageWeb({
    Key? key,
    required List<DestinationImageEntity> itemExplorerImage,
    required PageController controller,
    required Function() nextPages,
    required Function(int) onChanged,
    required Function() prevPages,
  })  : _itemExplorerImage = itemExplorerImage,
        _nextPages = nextPages,
        _controller = controller,
        _onChanged = onChanged,
        _prevPages = prevPages,
        super(key: key);

  final List<DestinationImageEntity> _itemExplorerImage;
  final PageController _controller;
  final Function() _nextPages;
  final Function(int) _onChanged;
  final Function() _prevPages;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      height: ResponsiveUtils(context).getResponsiveSize(637, desktop: 915),
      padding: EdgeInsets.only(
        top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
            AppGap.extraBig,
        bottom: ResponsiveUtils(context).getMediaQueryPaddingBottom() +
            AppGap.giant,
        left: AppGap.giant,
        right: ResponsiveUtils(context)
            .getResponsiveSize(AppGap.medium, desktop: AppGap.giant),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "TOP DESTINATION",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.secondary,
              fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                AppFontSize.normal,
                tablet: AppFontSize.normal,
                desktop: AppFontSize.medium,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Explore Top Destination",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                    AppFontSize.extraBig,
                    desktop: AppFontSize.big + AppFontSize.extraSmall,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    width: ResponsiveUtils(context)
                        .getResponsiveIconSize(46, desktop: 60),
                    height: ResponsiveUtils(context)
                        .getResponsiveIconSize(46, desktop: 60),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.greyShade, width: 1),
                      shape: BoxShape.circle,
                      color: AppColors.white,
                    ),
                    child: IconButton(
                      onPressed: _prevPages,
                      icon: const Icon(
                        Icons.arrow_back_rounded,
                        size: AppIconSize.normal,
                      ),
                    ),
                  ),
                  const Gap(height: AppGap.big),
                  Container(
                    width: ResponsiveUtils(context)
                        .getResponsiveIconSize(46, desktop: 60),
                    height: ResponsiveUtils(context)
                        .getResponsiveIconSize(46, desktop: 60),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.greyShade, width: 1),
                      shape: BoxShape.circle,
                      color: AppColors.primary,
                    ),
                    child: IconButton(
                      onPressed: _nextPages,
                      icon: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.white,
                        size: AppIconSize.normal,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          Gap(
            height: ResponsiveUtils(context)
                .getResponsiveSize(AppGap.big, desktop: AppGap.extraBig),
          ),
          Expanded(
            child: PageView.builder(
              controller: _controller,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: (_itemExplorerImage.length / 3).ceil(),
              onPageChanged: _onChanged,
              itemBuilder: (context, index) {
                int start = index * 3;
                int end = (index + 1) * 3;
                end = end > _itemExplorerImage.length
                    ? _itemExplorerImage.length
                    : end;
                return ListView.builder(
                  itemCount: end - start,
                  physics: const NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, subIndex) {
                    return BuildCardItemWeb(
                      width: ResponsiveUtils(context)
                          .getResponsiveSize(195, desktop: 352),
                      image: _itemExplorerImage[start + subIndex].images,
                      title: _itemExplorerImage[start + subIndex].title,
                      subtitle: _itemExplorerImage[start + subIndex].subtitle,
                      price: _itemExplorerImage[start + subIndex].price,
                    );
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
