import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:responsive_ui/feature/common/common.dart';
import 'package:responsive_ui/feature/services/data/entities/service_image_entity.dart';

class ServicePages extends StatefulWidget {
  const ServicePages({super.key});

  @override
  State<ServicePages> createState() => _ServicePagesState();
}

class _ServicePagesState extends State<ServicePages> {
  final List<ServiceImageEntity> _getServiceList =
      ServiceImageEntity.getImageServiceList;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        if (constraints.maxWidth > 600) {
          return _BodyServiceWeb(
            itemImage: _getServiceList,
          );
        } else {
          return _BodyServiceMobile(
            itemImage: _getServiceList,
          );
        }
      },
    );
  }
}

class _BodyServiceMobile extends StatelessWidget {
  const _BodyServiceMobile({
    Key? key,
    required List<ServiceImageEntity> itemImage,
  })  : _itemImage = itemImage,
        super(key: key);

  final List<ServiceImageEntity> _itemImage;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      height: 998,
      child: ColumnPadding(
        padding: const EdgeInsets.symmetric(
          vertical: AppGap.big + AppGap.tiny,
          horizontal: AppGap.medium,
        ),
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "SERVICES",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.secondary,
              fontSize: ResponsiveUtils(context).getResponsiveSize(
                AppFontSize.large,
                small: AppFontSize.normal,
                tablet: AppFontSize.extraLarge,
                desktop: AppFontSize.big,
              ),
            ),
          ),
          const Gap(height: AppGap.medium),
          Text(
            "Our top value categories \nfor you",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.black,
              fontSize: ResponsiveUtils(context).getResponsiveSize(
                  AppFontSize.big,
                  small: AppFontSize.large,
                  tablet: AppFontSize.ultra,
                  desktop: AppFontSize.extraUltra),
            ),
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.medium),
          Expanded(
            child: ListView.builder(
              itemCount: _itemImage.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Container(
                  padding: const EdgeInsets.only(
                      left: AppGap.big,
                      top: AppGap.big,
                      right: AppGap.big,
                      bottom: AppGap.normal),
                  margin: const EdgeInsets.only(bottom: AppGap.medium),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AppBorderRadius.big),
                    border: Border.all(color: AppColors.greyShade, width: 1),
                    color: AppColors.white,
                  ),
                  child: Column(
                    children: [
                      CustomImageWrapper(
                        image: _itemImage[index].image,
                        width: 64,
                        height: 64,
                        fit: BoxFit.contain,
                        isNetworkImage: false,
                      ),
                      const Gap(height: AppGap.big),
                      Text(
                        _itemImage[index].title,
                        style: AppTextStyle.bold.copyWith(
                          color: AppColors.black,
                          fontSize: AppFontSize.big,
                        ),
                      ),
                      const Gap(height: AppGap.big),
                      Text(
                        _itemImage[index].desc,
                        style: AppTextStyle.regular.copyWith(
                          color: AppColors.grey,
                          fontSize: AppFontSize.large,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class _BodyServiceWeb extends StatelessWidget {
  const _BodyServiceWeb({
    Key? key,
    required List<ServiceImageEntity> itemImage,
  })  : _itemImage = itemImage,
        super(key: key);

  final List<ServiceImageEntity> _itemImage;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      padding: const EdgeInsets.only(left: AppGap.giant),
      child: Row(
        children: [
          SizedBox(
            width: ResponsiveUtils(context)
                .getResponsiveSize(300, tablet: 240, desktop: 500),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "SERVICES",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.secondary,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                      AppFontSize.normal,
                      tablet: AppFontSize.normal,
                      desktop: AppFontSize.medium,
                    ),
                  ),
                ),
                Text(
                  "Our top value \ncategories for you",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                      AppFontSize.extraBig,
                      desktop: AppFontSize.big + AppFontSize.small,
                    ),
                  ),
                  maxLines: 2,
                )
              ],
            ),
          ),
          const Gap(width: AppGap.medium),
          Expanded(
            child: SizedBox(
              width:
                  ResponsiveUtils(context).getResponsiveSize(250, desktop: 500),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                dragStartBehavior: DragStartBehavior.start,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(_itemImage.length, (index) {
                    return Container(
                      width: ResponsiveUtils(context)
                          .getResponsiveSize(290, desktop: 350),
                      height: ResponsiveUtils(context)
                          .getResponsiveSize(363, desktop: 463),
                      margin: EdgeInsets.only(
                        bottom: AppGap.extraBig,
                        top: ResponsiveUtils(context).getResponsiveSize(
                          AppGap.large,
                          desktop: AppGap.extraBig,
                        ),
                        right: AppGap.large,
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(AppBorderRadius.big),
                        border:
                            Border.all(color: AppColors.greyShade, width: 1),
                        color: AppColors.white,
                      ),
                      child: ColumnPadding(
                        padding: const EdgeInsets.all(AppGap.extraBig),
                        children: [
                          CustomImageWrapper(
                            image: _itemImage[index].image,
                            width:
                                ResponsiveUtils(context).getResponsiveLogoSize(
                              44,
                              desktop: 64,
                            ),
                            height:
                                ResponsiveUtils(context).getResponsiveLogoSize(
                              44,
                              desktop: 64,
                            ),
                            fit: BoxFit.contain,
                            isNetworkImage: false,
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.big,
                              desktop: AppGap.extraBig,
                            ),
                          ),
                          Text(
                            _itemImage[index].title,
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.black,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.large,
                                desktop: AppFontSize.big,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.big,
                              desktop: AppGap.extraBig,
                            ),
                          ),
                          Text(
                            _itemImage[index].desc,
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.grey,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                desktop: AppFontSize.large,
                              ),
                            ),
                            textAlign: TextAlign.center,
                          ),
                          const Gap(height: AppGap.big),
                        ],
                      ),
                    );
                  }),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
