class ServicePagesParams {
  String images;
  String title;
  String desc;

  ServicePagesParams({
    this.images = '',
    this.title = '',
    this.desc = '',
  });
}
