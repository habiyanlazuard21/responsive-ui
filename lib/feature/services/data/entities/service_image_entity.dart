import 'package:responsive_ui/feature/common/utils/assets.dart';

class ServiceImageEntity {
  final String image;
  final String title;
  final String desc;

  ServiceImageEntity(
    this.image,
    this.title,
    this.desc,
  );

  static List<ServiceImageEntity> getImageServiceList = [
    ServiceImageEntity(
      AppIcon.destination,
      "Best Tour Guide",
      "What looked like a small patch of purple grass, above five feet.",
    ),
    ServiceImageEntity(
      AppIcon.booking,
      "Easy Booking",
      "Square, was moving across the sand in their direction.",
    ),
    ServiceImageEntity(
      AppIcon.colud,
      "Weather Forecast",
      "What looked like a small patch of purple grass, above five feet.",
    ),
  ];
}
