import 'package:flutter/material.dart';

import '../../../../lib.dart';

class TravelPointScreen extends StatefulWidget {
  const TravelPointScreen({super.key});

  @override
  State<TravelPointScreen> createState() => _TravelPointScreenState();
}

class _TravelPointScreenState extends State<TravelPointScreen> {
  final String _desc =
      "Contrary to popular belief, Lorem Ipsum is not \nsimply random text. It has roots in a piece of \nclassical Latin literature from 45 BC.";
  final List<TravelPointEntity> _getTravelPointList =
      TravelPointEntity.getTravelPointList;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        if (constraints.maxWidth < 600) {
          return _BodyTravelPointMobile(
            desc: _desc,
            getTravelList: _getTravelPointList,
          );
        } else {
          return _BodyTravelPointWeb(
            desc: _desc,
            getTravelPointEntity: _getTravelPointList,
          );
        }
      },
    );
  }
}

class _BodyTravelPointMobile extends StatelessWidget {
  const _BodyTravelPointMobile({
    Key? key,
    String desc = "",
    required List<TravelPointEntity> getTravelList,
  })  : _desc = desc,
        _getTravelList = getTravelList,
        super(key: key);
  final String _desc;
  final List<TravelPointEntity> _getTravelList;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      padding: const EdgeInsets.symmetric(horizontal: AppGap.medium),
      child: Column(
        children: [
          const CustomImageWrapper(
            image: AppImages.travelPointMobile,
            width: 871,
            height: 557,
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
          Text(
            "TRAVEL POINT",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.secondary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.medium),
          Text(
            "We helping you find your \ndream location",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.black,
              fontSize: AppFontSize.ultra,
            ),
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.big),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.grey,
              fontSize: AppFontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.big),
          ListView.builder(
            itemCount: _getTravelList.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Container(
                width: double.infinity,
                height: 151,
                margin: const EdgeInsets.only(bottom: AppGap.medium),
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.greyShade, width: 1),
                  borderRadius: BorderRadius.circular(AppGap.big),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      _getTravelList[index].point,
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.orange,
                        fontSize: AppFontSize.ultra,
                      ),
                    ),
                    const Gap(height: AppGap.medium),
                    Text(
                      _getTravelList[index].titlePoint,
                      style: AppTextStyle.regular.copyWith(
                        color: AppColors.grey,
                        fontSize: AppFontSize.large,
                      ),
                    ),
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}

class _BodyTravelPointWeb extends StatelessWidget {
  const _BodyTravelPointWeb({
    Key? key,
    String desc = "",
    required List<TravelPointEntity> getTravelPointEntity,
  })  : _desc = desc,
        _getTravelPointList = getTravelPointEntity,
        super(key: key);

  final String _desc;
  final List<TravelPointEntity> _getTravelPointList;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: ResponsiveUtils(context).getResponsiveSize(
        475,
        desktop:
            ResponsiveUtils(context).getMediaQueryWidth() > 1185 ? 817 : 617,
      ),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            child: Visibility(
              visible: ResponsiveUtils(context).isTabletSize(),
              child: CustomImageWrapper(
                image: AppImages.travelPointWeb,
                width: ResponsiveUtils(context).getResponsiveSize(
                  421,
                  desktop: 821,
                ),
                height: ResponsiveUtils(context).getResponsiveSize(
                  407,
                  desktop: 807,
                ),
                isNetworkImage: false,
              ),
            ),
          ),
          Positioned(
            right: 10,
            top: 0,
            child: Visibility(
              visible: ResponsiveUtils(context).isTabletSize(),
              child: Container(
                width: ResponsiveUtils(context).getMediaQueryWidth() * 0.35,
                height: 634,
                margin:
                    const EdgeInsets.only(top: AppGap.big, right: AppGap.big),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "TRAVEL POINT",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.secondary,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.normal,
                          tablet: AppFontSize.small,
                          desktop: AppFontSize.large,
                        ),
                      ),
                    ),
                    const Gap(height: AppGap.normal),
                    Text(
                      "We helping you find \nyour dream location",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.black,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.big,
                          desktop: AppFontSize.big + AppFontSize.small,
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                        AppGap.medium,
                        desktop: AppGap.big,
                      ),
                    ),
                    Text(
                      _desc,
                      style: AppTextStyle.regular.copyWith(
                        color: AppColors.grey,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.small,
                          desktop: AppFontSize.large,
                        ),
                      ),
                      overflow: TextOverflow.clip,
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                        AppGap.medium,
                        desktop: AppGap.big,
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 251,
                      child: GridView.builder(
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(AppGap.small),
                        itemCount: _getTravelPointList.length,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: AppGap.normal,
                                crossAxisSpacing: AppGap.normal,
                                childAspectRatio: 3 / 2),
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                AppBorderRadius.normal,
                              ),
                              border:
                                  Border.all(color: AppColors.grey, width: 1),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  _getTravelPointList[index].point,
                                  style: AppTextStyle.bold.copyWith(
                                    color: AppColors.orange,
                                    fontSize: ResponsiveUtils(context)
                                        .getResponsiveFontSize(
                                      AppFontSize.normal,
                                      desktop: AppFontSize.big,
                                    ),
                                  ),
                                ),
                                const Gap(height: AppGap.extraSmall),
                                Text(
                                  _getTravelPointList[index].titlePoint,
                                  style: AppTextStyle.regular.copyWith(
                                    color: AppColors.black,
                                    fontSize: ResponsiveUtils(context)
                                        .getResponsiveFontSize(
                                      AppFontSize.extraSmall,
                                      desktop: AppFontSize.large,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: ResponsiveUtils(context).isDesktopSize(),
            child: Container(
              width: double.infinity,
              margin: const EdgeInsets.only(right: AppGap.large),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Visibility(
                    visible:
                        ResponsiveUtils(context).getMediaQueryWidth() > 1185,
                    child: CustomImageWrapper(
                      image: AppImages.travelPointWeb,
                      width:
                          ResponsiveUtils(context).getMediaQueryWidth() * 0.58,
                      height: 621,
                      isNetworkImage: false,
                    ),
                  ),
                  Visibility(
                    visible:
                        ResponsiveUtils(context).getMediaQueryWidth() < 1185,
                    child: CustomImageWrapper(
                      image: AppImages.travelPointWeb,
                      width:
                          ResponsiveUtils(context).getMediaQueryWidth() * 0.54,
                      height: 481,
                      isNetworkImage: false,
                    ),
                  ),
                  Visibility(
                    visible:
                        ResponsiveUtils(context).getMediaQueryWidth() > 1185,
                    child: Container(
                      width: 341,
                      margin: const EdgeInsets.only(right: AppGap.giant),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "TRAVEL POINT",
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.secondary,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                tablet: AppFontSize.small,
                                desktop: AppFontSize.medium,
                              ),
                            ),
                          ),
                          const Gap(height: AppGap.normal),
                          Text(
                            "We helping you find \nyour dream location",
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.black,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.big,
                                desktop: AppFontSize.ultra,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.medium,
                              desktop: AppGap.medium,
                            ),
                          ),
                          Text(
                            _desc,
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.grey,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.small,
                                desktop: AppFontSize.medium,
                              ),
                            ),
                            overflow: TextOverflow.clip,
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.medium,
                              desktop: AppGap.big,
                            ),
                          ),
                          SizedBox(
                            width:
                                ResponsiveUtils(context).getMediaQueryWidth() *
                                    0.55,
                            height: 281,
                            child: GridView.builder(
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(AppGap.small),
                              itemCount: _getTravelPointList.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: AppGap.normal,
                                crossAxisSpacing: AppGap.normal,
                                childAspectRatio: 1.4 / 0.8,
                              ),
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      AppBorderRadius.normal,
                                    ),
                                    border: Border.all(
                                        color: AppColors.grey, width: 1),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        _getTravelPointList[index].point,
                                        style: AppTextStyle.bold.copyWith(
                                          color: AppColors.orange,
                                          fontSize: ResponsiveUtils(context)
                                              .getResponsiveFontSize(
                                            AppFontSize.normal,
                                            desktop: AppFontSize.medium,
                                          ),
                                        ),
                                      ),
                                      const Gap(height: AppGap.extraSmall),
                                      Text(
                                        _getTravelPointList[index].titlePoint,
                                        style: AppTextStyle.regular.copyWith(
                                          color: AppColors.black,
                                          fontSize: ResponsiveUtils(context)
                                              .getResponsiveFontSize(
                                            AppFontSize.extraSmall,
                                            desktop: AppFontSize.normal,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible:
                        ResponsiveUtils(context).getMediaQueryWidth() < 1185,
                    child: Container(
                      width: 281,
                      margin: const EdgeInsets.only(
                          right: AppGap.medium, top: AppGap.medium),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "TRAVEL POINT",
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.secondary,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                tablet: AppFontSize.small,
                                desktop: AppFontSize.normal,
                              ),
                            ),
                          ),
                          const Gap(height: AppGap.normal),
                          Text(
                            "We helping you find \nyour dream location",
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.black,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.big,
                                desktop: AppFontSize.big,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.medium,
                              desktop: AppGap.medium,
                            ),
                          ),
                          Text(
                            _desc,
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.grey,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.small,
                                desktop: AppFontSize.normal,
                              ),
                            ),
                            overflow: TextOverflow.clip,
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.medium,
                              desktop: AppGap.big,
                            ),
                          ),
                          SizedBox(
                            width:
                                ResponsiveUtils(context).getMediaQueryWidth() *
                                    0.55,
                            height: 281,
                            child: GridView.builder(
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(AppGap.small),
                              itemCount: _getTravelPointList.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: AppGap.normal,
                                crossAxisSpacing: AppGap.normal,
                                childAspectRatio: 0.6 / 0.4,
                              ),
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                      AppBorderRadius.normal,
                                    ),
                                    border: Border.all(
                                        color: AppColors.grey, width: 1),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        _getTravelPointList[index].point,
                                        style: AppTextStyle.bold.copyWith(
                                          color: AppColors.orange,
                                          fontSize: ResponsiveUtils(context)
                                              .getResponsiveFontSize(
                                            AppFontSize.normal,
                                            desktop: AppFontSize.normal,
                                          ),
                                        ),
                                      ),
                                      const Gap(height: AppGap.extraSmall),
                                      Text(
                                        _getTravelPointList[index].titlePoint,
                                        style: AppTextStyle.regular.copyWith(
                                          color: AppColors.black,
                                          fontSize: ResponsiveUtils(context)
                                              .getResponsiveFontSize(
                                            AppFontSize.extraSmall,
                                            desktop: AppFontSize.small,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
