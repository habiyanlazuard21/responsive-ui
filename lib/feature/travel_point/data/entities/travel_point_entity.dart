class TravelPointEntity {
  final String point;
  final String titlePoint;

  TravelPointEntity(this.point, this.titlePoint);

  static List<TravelPointEntity> getTravelPointList = [
    TravelPointEntity("500+", "Holiday Package"),
    TravelPointEntity("100", "Luxury Hotel"),
    TravelPointEntity("7", "Platinum Airlanes"),
    TravelPointEntity("2k+", "Happy Customer"),
  ];
}
