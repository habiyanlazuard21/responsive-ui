import 'package:flutter/material.dart';
import 'package:responsive_ui/feature/testimonial/data/entities/testimony_entity.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../lib.dart';

class TestimonialScreen extends StatefulWidget {
  const TestimonialScreen({super.key});

  @override
  State<TestimonialScreen> createState() => _TestimonialScreenState();
}

class _TestimonialScreenState extends State<TestimonialScreen> {
  late PageController _controller;
  final List<TestimonyEntity> _getTestimonyList =
      TestimonyEntity.getTestimonyList;
  int _currentPages = 0;

  @override
  void initState() {
    _controller = PageController(initialPage: _currentPages);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onChanged(int index) {
    setState(() {
      _currentPages = index;
    });
  }

  void _nextPages() {
    if (_currentPages < _getTestimonyList.length - 1) {
      _controller.nextPage(
        duration: const Duration(milliseconds: 300),
        curve: Curves.bounceInOut,
      );
    }
  }

  void _prevPages() {
    if (_currentPages > 0) {
      _controller.previousPage(
        duration: const Duration(milliseconds: 300),
        curve: Curves.bounceInOut,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 600) {
          return _TestimonialScreenMobile(
            controller: _controller,
            onChanged: _onChanged,
            nextPages: _nextPages,
            prevPages: _prevPages,
            testimonyList: _getTestimonyList,
          );
        } else {
          return _TestimonialScreenWeb(
            controller: _controller,
            onChanged: _onChanged,
            testimonyList: _getTestimonyList,
            nextPages: _nextPages,
            prevPages: _prevPages,
          );
        }
      },
    );
  }
}

class _TestimonialScreenMobile extends StatefulWidget {
  const _TestimonialScreenMobile({
    Key? key,
    required PageController controller,
    required Function(int) onChanged,
    Function()? prevPages,
    Function()? nextPages,
    required List<TestimonyEntity> testimonyList,
  })  : _onChanged = onChanged,
        _prevPages = prevPages,
        _nextPages = nextPages,
        _controller = controller,
        _testimonyList = testimonyList,
        super(key: key);

  final PageController _controller;
  final Function(int) _onChanged;
  final Function()? _prevPages;
  final Function()? _nextPages;
  final List<TestimonyEntity> _testimonyList;

  @override
  State<_TestimonialScreenMobile> createState() =>
      _TestimonialScreenMobileState();
}

class _TestimonialScreenMobileState extends State<_TestimonialScreenMobile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 666,
            child: PageView.builder(
              controller: widget._controller,
              onPageChanged: widget._onChanged,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: widget._testimonyList.length,
              itemBuilder: (context, index) {
                return _BuildTestimonyItem(
                  title: widget._testimonyList[index].title,
                  position: widget._testimonyList[index].position,
                  desc: widget._testimonyList[index].desc,
                );
              },
            ),
          ),
          SmoothPageIndicator(
            controller: widget._controller,
            count: widget._testimonyList.length,
            effect: const WormEffect(
              dotHeight: 16,
              dotWidth: 16,
              activeDotColor: AppColors.secondary,
              type: WormType.underground,
            ),
          ),
          const Gap(height: AppGap.big),
          RowPadding(
            padding: const EdgeInsets.symmetric(
                horizontal: AppBorderRadius.medium,
                vertical: AppBorderRadius.medium),
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 64,
                height: 64,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.greyShade, width: 1),
                  shape: BoxShape.circle,
                  color: AppColors.white,
                ),
                child: IconButton(
                  onPressed: widget._prevPages,
                  icon: const Icon(
                    Icons.arrow_back_rounded,
                    size: AppIconSize.normal,
                  ),
                ),
              ),
              Container(
                width: 64,
                height: 64,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.greyShade, width: 1),
                  shape: BoxShape.circle,
                  color: AppColors.primary,
                ),
                child: IconButton(
                  onPressed: widget._nextPages,
                  icon: const Icon(
                    Icons.arrow_forward,
                    color: AppColors.white,
                    size: AppIconSize.normal,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class _BuildTestimonyItem extends StatelessWidget {
  const _BuildTestimonyItem({
    Key? key,
    required String title,
    required String position,
    required String desc,
  })  : _title = title,
        _position = position,
        _desc = desc,
        super(key: key);

  final String _title;
  final String _position;
  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "TESTIMONIALS",
          style: AppTextStyle.bold.copyWith(
            color: AppColors.secondary,
            fontSize: AppFontSize.medium,
          ),
        ),
        Text(
          "Trust our clients",
          style: AppTextStyle.bold.copyWith(
            color: AppColors.black,
            fontSize: AppFontSize.big + AppFontSize.small,
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(
              vertical: AppGap.dialog, horizontal: AppGap.giant),
          child: CustomImageWrapper(
            image: AppImages.testimonials,
            width: 128,
            height: 128,
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
        ),
        Text.rich(
          TextSpan(
              text: _title,
              style: AppTextStyle.bold.copyWith(
                color: AppColors.orange,
                fontSize: AppFontSize.extraBig,
              ),
              children: [
                TextSpan(
                  text: _position,
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.grey,
                    fontSize: AppFontSize.extraBig,
                  ),
                )
              ]),
        ),
        const Gap(height: AppGap.big + AppGap.tiny),
        RowPadding(
          padding: const EdgeInsets.symmetric(horizontal: AppGap.extraBig),
          mainAxisAlignment: ResponsiveUtils(context).getMediaQueryWidth() < 480
              ? MainAxisAlignment.spaceEvenly
              : MainAxisAlignment.center,
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: AppIconSize.extraLarge,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: AppIconSize.extraLarge,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: AppIconSize.extraLarge,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: AppIconSize.extraLarge,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: AppIconSize.extraLarge,
              ),
            ),
          ],
        ),
        const Gap(height: AppGap.big + AppGap.tiny),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppGap.big),
          child: Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.grey,
              fontSize: AppFontSize.large,
            ),
            maxLines: 3,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}

class _TestimonialScreenWeb extends StatelessWidget {
  const _TestimonialScreenWeb({
    Key? key,
    required PageController controller,
    required Function(int) onChanged,
    Function()? prevPages,
    Function()? nextPages,
    required List<TestimonyEntity> testimonyList,
  })  : _controller = controller,
        _onChanged = onChanged,
        _prevPages = prevPages,
        _nextPages = nextPages,
        _testimonyList = testimonyList,
        super(key: key);

  final PageController _controller;
  final Function(int) _onChanged;
  final Function()? _prevPages;
  final Function()? _nextPages;
  final List<TestimonyEntity> _testimonyList;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 0,
            child: CustomImageWrapper(
              image: AppImages.bgTestimonial,
              width: ResponsiveUtils(context).getMediaQueryWidth(),
              height: ResponsiveUtils(context).getMediaQueryHeight() * 1.3,
              fit: BoxFit.fitWidth,
              isNetworkImage: false,
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(
              top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 128,
            ),
            width: ResponsiveUtils(context).getMediaQueryWidth(),
            height: 845,
            child: PageView.builder(
              controller: _controller,
              onPageChanged: _onChanged,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: _testimonyList.length,
              itemBuilder: (context, index) {
                return _BuildTestimonyItem(
                  title: _testimonyList[index].title,
                  position: _testimonyList[index].position,
                  desc: _testimonyList[index].desc,
                );
              },
            ),
          ),
          Positioned(
            bottom: AppGap.giant,
            child: SmoothPageIndicator(
              controller: _controller,
              count: _testimonyList.length,
              effect: const WormEffect(
                dotHeight: 16,
                dotWidth: 16,
                activeDotColor: AppColors.secondary,
                type: WormType.underground,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: ResponsiveUtils(context).getResponsivePosition(
                AppGap.medium,
                desktop: AppGap.giant,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 64,
                  height: 64,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.greyShade, width: 1),
                    shape: BoxShape.circle,
                    color: AppColors.white,
                  ),
                  child: IconButton(
                    onPressed: _prevPages,
                    icon: const Icon(
                      Icons.arrow_back,
                      color: AppColors.black,
                      size: AppIconSize.normal,
                    ),
                  ),
                ),
                Container(
                  width: 64,
                  height: 64,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.greyShade, width: 1),
                    shape: BoxShape.circle,
                    color: AppColors.primary,
                  ),
                  child: IconButton(
                    onPressed: _nextPages,
                    icon: const Icon(
                      Icons.arrow_forward,
                      color: AppColors.white,
                      size: AppIconSize.normal,
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
