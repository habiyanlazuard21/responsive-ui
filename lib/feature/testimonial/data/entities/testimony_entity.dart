class TestimonyEntity {
  final String title;
  final String position;
  final String desc;

  TestimonyEntity(this.title, this.position, this.desc);

  static List<TestimonyEntity> getTestimonyList = [
    TestimonyEntity(
      "Mark Smith",
      " / Travel Enthusiast",
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots \nin a piece of classical Latin literature from 45 BC.",
    ),
    TestimonyEntity(
      "Mark Smith",
      " / Travel Enthusiast",
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots \nin a piece of classical Latin literature from 45 BC.",
    ),
    TestimonyEntity(
      "Mark Smith",
      " / Travel Enthusiast",
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots \nin a piece of classical Latin literature from 45 BC.",
    )
  ];
}
