import 'package:flutter/material.dart';

import '../../../../lib.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({super.key});

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  final String _desc =
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.";

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 800) {
          return _AboutScreenMobile(
            desc: _desc,
          );
        } else {
          return _AboutScreenWeb();
        }
      },
    );
  }
}

class _AboutScreenMobile extends StatelessWidget {
  const _AboutScreenMobile({
    Key? key,
    String desc = "",
  })  : _desc = desc,
        super(key: key);

  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      padding: const EdgeInsets.symmetric(horizontal: AppGap.medium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const CustomImageWrapper(
                image: AppIcon.logo,
                width: 40,
                height: 40,
                isNetworkImage: false,
              ),
              const Gap(width: AppGap.medium),
              Text(
                "Travlog",
                style: AppTextStyle.black.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              )
            ],
          ),
          const Gap(height: AppGap.big),
          SizedBox(
            width: 398,
            child: Text(
              _desc,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.grey,
                fontSize: AppFontSize.medium,
              ),
            ),
          ),
          const Gap(height: AppGap.big),
          SizedBox(
            width: 160,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                CustomImageWrapper(
                  image: AppIcon.facebook,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                ),
                CustomImageWrapper(
                  image: AppIcon.twitter,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                ),
                CustomImageWrapper(
                  image: AppIcon.instagram,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                )
              ],
            ),
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Company",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Contact Us",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Meet Us",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
        ],
      ),
    );
  }
}

class _AboutScreenWeb extends StatelessWidget {
  const _AboutScreenWeb({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
