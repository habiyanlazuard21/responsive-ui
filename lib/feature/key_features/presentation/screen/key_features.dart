import 'package:flutter/material.dart';

import '../../../../lib.dart';

class KeyFeatures extends StatefulWidget {
  const KeyFeatures({super.key});

  @override
  State<KeyFeatures> createState() => _KeyFeaturesState();
}

class _KeyFeaturesState extends State<KeyFeatures> {
  final String _desc =
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.";
  final String _descWeb =
      "Contrary to popular belief, Lorem Ipsum is not simply random \ntext. It has roots in a piece of classical Latin literature \nfrom 45 BC.";
  final List<KeyFeaturesListEntity> _getKeyFeaturesList =
      KeyFeaturesListEntity.getKeyFeaturesList;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        if (constraints.maxWidth < 600) {
          return _KeyFeaturesMobile(
            desc: _desc,
          );
        } else {
          return _KeyFeaturesWeb(
            desc: _descWeb,
            getKeyFeaturesList: _getKeyFeaturesList,
          );
        }
      },
    );
  }
}

class _KeyFeaturesMobile extends StatelessWidget {
  const _KeyFeaturesMobile({
    Key? key,
    String desc = "",
  })  : _desc = desc,
        super(key: key);

  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: AppColors.white,
      padding: const EdgeInsets.only(
        left: AppGap.medium,
        top: AppGap.big,
        right: AppBorderRadius.medium,
      ),
      child: Column(
        children: [
          const CustomImageWrapper(
            image: AppImages.keyFeatures,
            width: 350,
            height: 438,
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
          const Gap(height: AppGap.dialog),
          Text(
            "KEY FEATURES",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.secondary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.medium),
          Text(
            "We offer best services",
            style: AppTextStyle.bold.copyWith(
              color: AppColors.black,
              fontSize: AppFontSize.big + AppFontSize.small,
            ),
          ),
          const Gap(height: AppGap.big),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.grey,
              fontSize: AppFontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.big),
          Container(
            width: 334,
            height: 290,
            margin: const EdgeInsets.symmetric(
              horizontal: AppGap.big + AppGap.tiny,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: AppColors.orange,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      AppGap.big + AppGap.tiny,
                    ),
                  ),
                  child: const Center(
                    child: CustomImageWrapper(
                      image: AppIcon.location,
                      width: 34,
                      height: 40,
                      fit: BoxFit.contain,
                      isNetworkImage: false,
                    ),
                  ),
                ),
                const Gap(height: AppGap.big),
                Text(
                  "We offer best services",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.big,
                  ),
                ),
                const Gap(height: AppGap.small),
                Text(
                  "Lorem Ipsum is not simply \nrandom text",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.grey,
                    fontSize: AppFontSize.large,
                  ),
                )
              ],
            ),
          ),
          Container(
            width: 334,
            height: 290,
            margin: const EdgeInsets.symmetric(
              horizontal: AppGap.big + AppGap.tiny,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: AppColors.yellow,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      AppGap.big + AppGap.tiny,
                    ),
                  ),
                  child: const Center(
                    child: CustomImageWrapper(
                      image: AppIcon.location,
                      width: 34,
                      height: 40,
                      fit: BoxFit.contain,
                      isNetworkImage: false,
                    ),
                  ),
                ),
                const Gap(height: AppGap.big),
                Text(
                  "Schedule your trip",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.big,
                  ),
                ),
                const Gap(height: AppGap.small),
                Text(
                  "It has roots in a piece of \nclassical",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.grey,
                    fontSize: AppFontSize.large,
                  ),
                )
              ],
            ),
          ),
          Container(
            width: 334,
            height: 290,
            margin: const EdgeInsets.symmetric(
              horizontal: AppGap.big + AppGap.tiny,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: AppColors.secondary,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      AppGap.big + AppGap.tiny,
                    ),
                  ),
                  child: const Center(
                    child: CustomImageWrapper(
                      image: AppIcon.location,
                      width: 34,
                      height: 40,
                      fit: BoxFit.contain,
                      isNetworkImage: false,
                    ),
                  ),
                ),
                const Gap(height: AppGap.big),
                Text(
                  "Get discounted coupons",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.big,
                  ),
                ),
                const Gap(height: AppGap.small),
                Text(
                  "Lorem Ipsum is not simply \nrandom text",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.grey,
                    fontSize: AppFontSize.large,
                  ),
                ),
                const Gap(height: AppGap.big),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _KeyFeaturesWeb extends StatelessWidget {
  const _KeyFeaturesWeb({
    String desc = "",
    required List<KeyFeaturesListEntity> getKeyFeaturesList,
    Key? key,
  })  : _desc = desc,
        _getKeyFeaturesList = getKeyFeaturesList,
        super(key: key);

  final String _desc;
  final List<KeyFeaturesListEntity> _getKeyFeaturesList;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.only(
        left: ResponsiveUtils(context)
            .getResponsiveSize(AppGap.extraBig, desktop: AppGap.giant),
      ),
      child: Stack(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(
            visible: ResponsiveUtils(context).getMediaQueryWidth() > 1185,
            child: SizedBox(
              width:
                  ResponsiveUtils(context).getResponsiveSize(299, desktop: 529),
              height:
                  ResponsiveUtils(context).getResponsiveSize(442, desktop: 772),
              child: Transform.scale(
                alignment: Alignment.centerLeft,
                scale: ResponsiveUtils(context).getResponsivePosition(
                    1 * MediaQuery.of(context).size.aspectRatio * 0.85,
                    desktop: 1),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "KEY FEATURES",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.secondary,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.normal,
                          tablet: AppFontSize.normal,
                          desktop: AppFontSize.medium,
                        ),
                      ),
                    ),
                    Text(
                      "We offer best services",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.black,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.big,
                          desktop: AppFontSize.big + AppFontSize.small,
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                          AppGap.normal,
                          desktop: AppGap.big),
                    ),
                    Text(
                      _desc,
                      style: AppTextStyle.regular.copyWith(
                        color: AppColors.grey,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.extraSmall,
                          desktop: AppFontSize.large,
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                          AppGap.big,
                          desktop: AppGap.dialog),
                    ),
                    ...List.generate(_getKeyFeaturesList.length, (index) {
                      return _BuildCardItemFeatures(
                        backgroundIcon:
                            _getKeyFeaturesList[index].backgroundColor,
                        image: _getKeyFeaturesList[index].icon,
                        title: _getKeyFeaturesList[index].title,
                        subtitle: _getKeyFeaturesList[index].subtitle,
                      );
                    }),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: ResponsiveUtils(context).getMediaQueryWidth() < 1185,
            child: SizedBox(
              width:
                  ResponsiveUtils(context).getResponsiveSize(299, desktop: 429),
              height:
                  ResponsiveUtils(context).getResponsiveSize(442, desktop: 672),
              child: Transform.scale(
                alignment: Alignment.centerLeft,
                scale: ResponsiveUtils(context).getResponsivePosition(
                    1 * MediaQuery.of(context).size.aspectRatio * 0.80,
                    desktop: 1),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "KEY FEATURES",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.secondary,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.normal,
                          tablet: AppFontSize.normal,
                          desktop: AppFontSize.normal,
                        ),
                      ),
                    ),
                    Text(
                      "We offer best services",
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.black,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.big,
                          desktop: AppFontSize.extraBig,
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                          AppGap.normal,
                          desktop: AppGap.large),
                    ),
                    Text(
                      _desc,
                      style: AppTextStyle.regular.copyWith(
                        color: AppColors.grey,
                        fontSize:
                            ResponsiveUtils(context).getResponsiveFontSize(
                          AppFontSize.extraSmall,
                          desktop: AppFontSize.medium,
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                        AppGap.big,
                        desktop:
                            ResponsiveUtils(context).getMediaQueryWidth() < 1185
                                ? AppGap.big
                                : AppGap.dialog,
                      ),
                    ),
                    ...List.generate(_getKeyFeaturesList.length, (index) {
                      return _BuildCardItemFeatures(
                        backgroundIcon:
                            _getKeyFeaturesList[index].backgroundColor,
                        image: _getKeyFeaturesList[index].icon,
                        title: _getKeyFeaturesList[index].title,
                        subtitle: _getKeyFeaturesList[index].subtitle,
                      );
                    }),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: ResponsiveUtils(context).getMediaQueryWidth() > 1185,
            child: Positioned(
              right: AppGap.small,
              left: ResponsiveUtils(context)
                  .getResponsivePosition(287, desktop: 300),
              child: CustomImageWrapper(
                image: AppImages.keyFeatures,
                width: ResponsiveUtils(context)
                    .getResponsiveSize(356, desktop: 593),
                height: ResponsiveUtils(context)
                    .getResponsiveSize(434, desktop: 769),
                fit: BoxFit.contain,
                isNetworkImage: false,
              ),
            ),
          ),
          Visibility(
            visible: ResponsiveUtils(context).getMediaQueryWidth() < 1185,
            child: Positioned(
              right: AppGap.small,
              left: ResponsiveUtils(context)
                  .getResponsivePosition(287, desktop: 315),
              child: CustomImageWrapper(
                image: AppImages.keyFeatures,
                width: ResponsiveUtils(context)
                    .getResponsiveSize(356, desktop: 493),
                height: ResponsiveUtils(context)
                    .getResponsiveSize(434, desktop: 669),
                fit: BoxFit.contain,
                isNetworkImage: false,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _BuildCardItemFeatures extends StatelessWidget {
  const _BuildCardItemFeatures({
    Key? key,
    required Color backgroundIcon,
    required String image,
    required String title,
    required String subtitle,
  })  : _backgroundIcon = backgroundIcon,
        _image = image,
        _title = title,
        _subtitle = subtitle,
        super(key: key);

  final Color _backgroundIcon;
  final String _image;
  final String _title;
  final String _subtitle;

  @override
  Widget build(BuildContext context) {
    return RowPadding(
      crossAxisAlignment: CrossAxisAlignment.center,
      padding: EdgeInsets.only(
        bottom: ResponsiveUtils(context).getResponsivePadding(
          AppGap.big,
          desktop: ResponsiveUtils(context).getMediaQueryWidth() < 1185
              ? AppGap.big
              : AppGap.extraBig,
        ),
      ),
      children: [
        Container(
          width: ResponsiveUtils(context).getResponsiveSize(40, desktop: 60),
          height: ResponsiveUtils(context).getResponsiveSize(40, desktop: 60),
          decoration: BoxDecoration(
            color: _backgroundIcon,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(
              ResponsiveUtils(context)
                  .getResponsiveSize(AppGap.normal, desktop: AppGap.large),
            ),
          ),
          child: Center(
            child: CustomImageWrapper(
              image: _image,
              width: ResponsiveUtils(context)
                  .getResponsiveIconSize(22, desktop: 24),
              height: ResponsiveUtils(context)
                  .getResponsiveIconSize(14, desktop: 30),
              fit: BoxFit.contain,
              isNetworkImage: false,
            ),
          ),
        ),
        const Gap(width: AppGap.large),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _title,
              style: AppTextStyle.bold.copyWith(
                color: AppColors.black,
                fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                  AppFontSize.medium,
                  desktop: AppFontSize.extraLarge,
                ),
              ),
            ),
            Gap(
              height: ResponsiveUtils(context)
                  .getResponsiveSize(AppGap.tiny, desktop: AppGap.small),
            ),
            Text(
              _subtitle,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.grey,
                fontSize: AppFontSize.normal,
              ),
            ),
          ],
        )
      ],
    );
  }
}
