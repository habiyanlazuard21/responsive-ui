import 'package:flutter/material.dart';

import '../../../../lib.dart';

class KeyFeaturesListEntity {
  final Color backgroundColor;
  final String icon;
  final String title;
  final String subtitle;

  KeyFeaturesListEntity(
      this.backgroundColor, this.icon, this.title, this.subtitle);

  static List<KeyFeaturesListEntity> getKeyFeaturesList = [
    KeyFeaturesListEntity(
      AppColors.orange,
      AppIcon.location,
      "We offer best services",
      "Lorem Ipsum is not simply random text",
    ),
    KeyFeaturesListEntity(
      AppColors.yellow,
      AppIcon.calender,
      "Schedule your trip",
      "It has roots in a piece of classical",
    ),
    KeyFeaturesListEntity(
      AppColors.secondary,
      AppIcon.ticket,
      "Get discounted coupons",
      "Lorem Ipsum is not simply random text",
    )
  ];
}
