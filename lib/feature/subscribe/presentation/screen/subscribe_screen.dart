import 'package:flutter/material.dart';
import 'package:responsive_ui/feature/common/common.dart';

class SubscribeScreen extends StatefulWidget {
  const SubscribeScreen({super.key});

  @override
  State<SubscribeScreen> createState() => _SubscribeScreenState();
}

class _SubscribeScreenState extends State<SubscribeScreen> {
  final TextEditingController _controller = TextEditingController();
  final String _desc =
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.";

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 600) {
          return _SubscribeMobile(
            controller: _controller,
            desc: _desc,
          );
        } else {
          return _SubscribeWeb(
            desc: _desc,
          );
        }
      },
    );
  }
}

class _SubscribeMobile extends StatelessWidget {
  const _SubscribeMobile({
    Key? key,
    required TextEditingController controller,
    String desc = '',
  })  : _desc = desc,
        _controller = controller,
        super(key: key);

  final TextEditingController _controller;
  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            margin: const EdgeInsets.symmetric(
                horizontal: AppGap.medium, vertical: AppGap.medium),
            padding: const EdgeInsets.symmetric(
              vertical: AppGap.extraBig,
              horizontal: AppGap.large,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              color: AppColors.secondaryLight,
            ),
            child: Column(
              children: [
                Text(
                  "SUBSCRIBE TO OUR NEWSLETTER",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.secondary,
                    fontSize: AppFontSize.medium,
                  ),
                ),
                const Gap(height: AppGap.big),
                Text(
                  "Prepare yourself & let’s explore the beauty of the world",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.ultra,
                  ),
                  textAlign: TextAlign.center,
                ),
                const Gap(height: AppGap.extraBig),
                Container(
                  alignment: Alignment.center,
                  height: 74,
                  padding: const EdgeInsets.symmetric(horizontal: AppGap.big),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.circular(AppGap.medium),
                  ),
                  child: TextFormField(
                    cursorColor: AppColors.grey,
                    decoration: InputDecoration(
                      hintText: "Your Email",
                      hintStyle: AppTextStyle.bold.copyWith(
                        color: AppColors.grey,
                        fontSize: AppFontSize.medium,
                      ),
                      prefixIcon: const Icon(
                        Icons.email,
                        size: 24,
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                const Gap(height: AppGap.big),
                ButtonPrimary(
                  "Subscribe",
                  width: double.infinity,
                  borderRadius: AppBorderRadius.medium,
                  buttonColor: AppColors.primary,
                  height: 67,
                  onPressed: () {},
                ),
              ],
            ),
          ),

          /* body about */
          _bodyAboutMobile(desc: _desc),
        ],
      ),
    );
  }
}

class _SubscribeWeb extends StatelessWidget {
  const _SubscribeWeb({
    Key? key,
    String desc = "",
  })  : _desc = desc,
        super(key: key);

  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      height: ResponsiveUtils(context).getResponsiveSize(
        ResponsiveUtils(context).getMediaQueryHeight() * 1,
        desktop: ResponsiveUtils(context).getMediaQueryHeight() * 1.45,
      ),
      child: Stack(children: [
        Positioned(
          left: ResponsiveUtils(context).getResponsivePosition(53, desktop: 93),
          top: 0,
          child: CustomImageWrapper(
            image: AppImages.bgGraphic,
            width:
                ResponsiveUtils(context).getResponsiveSize(140, desktop: 183),
            height:
                ResponsiveUtils(context).getResponsiveSize(128, desktop: 176),
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
        ),
        Positioned(
          right: 0,
          top:
              ResponsiveUtils(context).getResponsivePosition(263, desktop: 323),
          child: CustomImageWrapper(
            image: AppImages.bgSideRight,
            width: ResponsiveUtils(context).getResponsiveSize(51, desktop: 102),
            height:
                ResponsiveUtils(context).getResponsiveSize(175, desktop: 351),
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
        ),
        Container(
          width: double.infinity,
          height: ResponsiveUtils(context).getResponsiveSize(308, desktop: 408),
          margin: EdgeInsets.only(
            left: ResponsiveUtils(context).getResponsiveSize(78, desktop: 128),
            top: 40,
            right: ResponsiveUtils(context)
                .getResponsiveSize(AppGap.extraBig, desktop: AppGap.giant),
          ),
          decoration: BoxDecoration(
            color: AppColors.secondaryLight,
            borderRadius: BorderRadius.circular(AppGap.big),
          ),
          child: ColumnPadding(
            padding: const EdgeInsets.symmetric(
              horizontal: AppGap.big,
              vertical: AppGap.big,
            ),
            children: [
              Text(
                "SUBSCRIBE TO OUR NEWSLETTER",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.secondary,
                  fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                    AppFontSize.normal,
                    tablet: AppFontSize.medium,
                    desktop: AppFontSize.medium,
                  ),
                ),
              ),
              const Gap(height: AppGap.big),
              Text(
                "Prepare yourself & let’s explore the \nbeauty of the world",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                    AppFontSize.big,
                    desktop: AppFontSize.big + AppFontSize.small,
                  ),
                ),
                textAlign: TextAlign.center,
              ),
              const Gap(height: AppGap.extraBig),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      alignment: Alignment.center,
                      width: ResponsiveUtils(context).getResponsiveSize(
                        ResponsiveUtils(context).getMediaQueryWidth() * 0.55,
                        desktop: 657,
                      ),
                      height: ResponsiveUtils(context)
                          .getResponsiveSize(55, desktop: 74),
                      padding:
                          const EdgeInsets.symmetric(horizontal: AppGap.large),
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(AppGap.medium),
                      ),
                      child: TextFormField(
                        cursorColor: AppColors.grey,
                        decoration: InputDecoration(
                          hintText: "Your Email",
                          hintStyle: AppTextStyle.bold.copyWith(
                            color: AppColors.grey,
                            fontSize: AppFontSize.medium,
                          ),
                          prefixIcon: const Icon(
                            Icons.email,
                            size: 24,
                          ),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const Gap(width: AppGap.large),
                  ButtonPrimary(
                    height: ResponsiveUtils(context)
                        .getResponsiveSize(55, desktop: 74),
                    width: ResponsiveUtils(context)
                        .getResponsiveSize(107, desktop: 235),
                    "Subscribe",
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                      AppFontSize.normal,
                      desktop: AppFontSize.big,
                    ),
                    buttonColor: AppColors.primary,
                    borderRadius: ResponsiveUtils(context).getResponsiveSize(
                        AppBorderRadius.medium,
                        desktop: AppBorderRadius.big),
                    onPressed: () {},
                  ),
                ],
              )
            ],
          ),
        ),

        /*  about */

        Container(
          alignment: Alignment.bottomCenter,
          width: double.infinity,
          height: ResponsiveUtils(context).getResponsiveSize(279, desktop: 379),
          margin: EdgeInsets.only(
            top: ResponsiveUtils(context).getResponsivePadding(
              ResponsiveUtils(context).getResponsivePaddingTop() + 330,
              desktop: ResponsiveUtils(context).getResponsivePaddingTop() + 480,
            ),
            left: ResponsiveUtils(context).getResponsiveSize(78, desktop: 128),
            right: AppGap.extraBig,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RowPadding(
                      padding: const EdgeInsets.only(bottom: AppGap.big),
                      children: [
                        const CustomImageWrapper(
                          width: 30,
                          height: 30,
                          image: AppIcon.logo,
                          isNetworkImage: false,
                        ),
                        const Gap(width: AppGap.normal),
                        Text(
                          "Travlog",
                          style: AppTextStyle.black.copyWith(
                            color: AppColors.black,
                            fontSize:
                                ResponsiveUtils(context).getResponsiveFontSize(
                              AppFontSize.large,
                              desktop: AppFontSize.big,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: ResponsiveUtils(context).getResponsiveSize(
                        260,
                        desktop: 460,
                      ),
                      child: Text(
                        _desc,
                        style: AppTextStyle.regular.copyWith(
                          color: AppColors.grey,
                          fontSize:
                              ResponsiveUtils(context).getResponsiveFontSize(
                            AppFontSize.small,
                            desktop: AppFontSize.big,
                          ),
                        ),
                      ),
                    ),
                    Gap(
                      height: ResponsiveUtils(context).getResponsiveSize(
                          AppGap.large,
                          desktop: AppGap.extraBig),
                    ),
                    SizedBox(
                      width: 160,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          CustomImageWrapper(
                            image: AppIcon.facebook,
                            width: 32,
                            height: 32,
                            isNetworkImage: false,
                          ),
                          CustomImageWrapper(
                            image: AppIcon.twitter,
                            width: 32,
                            height: 32,
                            isNetworkImage: false,
                          ),
                          CustomImageWrapper(
                            image: AppIcon.instagram,
                            width: 32,
                            height: 32,
                            isNetworkImage: false,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Company",
                          style: AppTextStyle.bold.copyWith(
                            color: AppColors.greyeScale,
                            fontSize:
                                ResponsiveUtils(context).getResponsiveFontSize(
                              AppFontSize.large,
                              desktop: AppFontSize.big,
                            ),
                          ),
                        ),
                        Gap(
                          height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.large,
                              desktop: AppGap.big),
                        ),
                        Text(
                          "About",
                          style: AppTextStyle.regular.copyWith(
                            color: AppColors.greyeScale,
                            fontSize:
                                ResponsiveUtils(context).getResponsiveFontSize(
                              AppFontSize.normal,
                              desktop: AppFontSize.large,
                            ),
                          ),
                        ),
                        Gap(
                          height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.large,
                              desktop: AppGap.big),
                        ),
                        Text(
                          "Career",
                          style: AppTextStyle.regular.copyWith(
                            color: AppColors.greyeScale,
                            fontSize:
                                ResponsiveUtils(context).getResponsiveFontSize(
                              AppFontSize.normal,
                              desktop: AppFontSize.large,
                            ),
                          ),
                        ),
                        Gap(
                          height: ResponsiveUtils(context).getResponsiveSize(
                              AppGap.large,
                              desktop: AppGap.big),
                        ),
                        Text(
                          "Mobile",
                          style: AppTextStyle.regular.copyWith(
                            color: AppColors.greyeScale,
                            fontSize:
                                ResponsiveUtils(context).getResponsiveFontSize(
                              AppFontSize.normal,
                              desktop: AppFontSize.large,
                            ),
                          ),
                        )
                      ],
                    ),
                    Flexible(
                      flex: 1,
                      child: Column(
                        children: [
                          Text(
                            "Contact",
                            style: AppTextStyle.bold.copyWith(
                              color: AppColors.greyeScale,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.large,
                                desktop: AppFontSize.big,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                                AppGap.large,
                                desktop: AppGap.big),
                          ),
                          Text(
                            "Why Travlog?",
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.greyeScale,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                desktop: AppFontSize.large,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                                AppGap.large,
                                desktop: AppGap.big),
                          ),
                          Text(
                            "Partner with us",
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.greyeScale,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                desktop: AppFontSize.large,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                                AppGap.large,
                                desktop: AppGap.big),
                          ),
                          Text(
                            "FAQ's",
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.greyeScale,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                desktop: AppFontSize.large,
                              ),
                            ),
                          ),
                          Gap(
                            height: ResponsiveUtils(context).getResponsiveSize(
                                AppGap.large,
                                desktop: AppGap.big),
                          ),
                          Text(
                            "Blog",
                            style: AppTextStyle.regular.copyWith(
                              color: AppColors.greyeScale,
                              fontSize: ResponsiveUtils(context)
                                  .getResponsiveFontSize(
                                AppFontSize.normal,
                                desktop: AppFontSize.large,
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ]),
    );
  }
}

class _bodyAboutMobile extends StatelessWidget {
  const _bodyAboutMobile({
    Key? key,
    String desc = "",
  })  : _desc = desc,
        super(key: key);

  final String _desc;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      padding: const EdgeInsets.symmetric(horizontal: AppGap.medium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const CustomImageWrapper(
                image: AppIcon.logo,
                width: 40,
                height: 40,
                isNetworkImage: false,
              ),
              const Gap(width: AppGap.medium),
              Text(
                "Travlog",
                style: AppTextStyle.black.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              )
            ],
          ),
          const Gap(height: AppGap.big),
          SizedBox(
            width: 398,
            child: Text(
              _desc,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.grey,
                fontSize: AppFontSize.medium,
              ),
            ),
          ),
          const Gap(height: AppGap.big),
          SizedBox(
            width: 160,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                CustomImageWrapper(
                  image: AppIcon.facebook,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                ),
                CustomImageWrapper(
                  image: AppIcon.twitter,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                ),
                CustomImageWrapper(
                  image: AppIcon.instagram,
                  width: 32,
                  height: 32,
                  isNetworkImage: false,
                )
              ],
            ),
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Company",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Contact Us",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Meet Us",
                style: AppTextStyle.bold.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.big,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.black, width: 1),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    size: AppIconSize.tiny,
                  ),
                ),
              )
            ],
          ),
          const Gap(height: AppGap.dialog + AppGap.small),
        ],
      ),
    );
  }
}
