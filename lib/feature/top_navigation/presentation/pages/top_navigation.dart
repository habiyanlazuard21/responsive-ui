import 'package:flutter/material.dart';

import '../../../../lib.dart';
import '../../../subscribe/presentation/screen/subscribe_screen.dart';

class TopNavigation extends StatefulWidget {
  const TopNavigation({
    Key? key,
  }) : super(key: key);

  @override
  State<TopNavigation> createState() => _TopNavigationState();
}

class _TopNavigationState extends State<TopNavigation>
    with TickerProviderStateMixin {
  final List<TopNavigationEntity> _topNavList =
      TopNavigationEntity.topNavigationEntity;
  int _activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) {
        return [
          SliverAppBar(
            pinned: false,
            elevation: 0,
            excludeHeaderSemantics: true,
            floating: true,
            titleSpacing: 20,
            stretch: true,
            centerTitle: true,
            expandedHeight: ResponsiveUtils(context).getResponsivePosition(114),
            flexibleSpace: ResponsiveUtils(context).getMediaQueryWidth() <= 600
                ? _topNavMobile(context)
                : _topNavigationWeb(context),
            backgroundColor: AppColors.white,
          )
        ];
      },
      body: CustomSingleChildScrollViewWrapper(
        child: Column(
          children: const [
            HomeScreen(),
            ServicePages(),
            DestinationPages(),
            TravelPointScreen(),
            KeyFeatures(),
            TestimonialScreen(),
            SubscribeScreen(),
          ],
        ),
      ),
    ));
  }

  Widget _topNavMobile(BuildContext ctx) {
    return Container(
      alignment: Alignment.center,
      margin: ResponsiveUtils(context).getMediaQueryWidth() <= 6000
          ? const EdgeInsets.symmetric(horizontal: AppGap.medium)
          : const EdgeInsets.only(left: AppGap.giant, right: AppGap.dialog),
      child: IntrinsicHeight(
        child: Stack(
          children: [
            RowPadding(
              padding: EdgeInsets.only(
                top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
                    AppGap.extraLarge,
                bottom: ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                    AppGap.extraLarge,
              ),
              children: [
                const AspectRatio(
                  aspectRatio: 3 / 4,
                  child: CustomImageWrapper(
                    image: AppIcon.logo,
                    width: 40,
                    height: 40,
                    fit: BoxFit.contain,
                    isNetworkImage: false,
                  ),
                ),
                const Gap(width: AppGap.normal),
                Text(
                  "Travlog",
                  style: AppTextStyle.black.copyWith(
                    color: AppColors.black,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                        AppFontSize.large,
                        tablet: AppFontSize.medium),
                  ),
                ),
              ],
            ),
            ResponsiveUtils(context).getMediaQueryWidth() < 680
                ? const SizedBox()
                : Align(
                    alignment: Alignment.center,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.center,
                      padding:
                          ResponsiveUtils(context).getMediaQueryWidth() < 775
                              ? const EdgeInsets.only(left: AppGap.extraBig)
                              : const EdgeInsets.only(right: AppGap.large),
                      children: [
                        ...List.generate(_topNavList.length, (index) {
                          return TextButton(
                            onPressed: () {
                              _activeIndex = index;
                            },
                            child: Text(
                              _topNavList[index].label,
                              style: AppTextStyle.bold.copyWith(
                                color: _activeIndex == index
                                    ? AppColors.black
                                    : AppColors.grey,
                                fontSize: AppFontSize.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          );
                        }),
                      ],
                    ),
                  ),
            ResponsiveUtils(context).getMediaQueryWidth() < 775
                ? const Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: AppGap.large,
                          bottom: AppGap.large,
                          right: AppGap.large),
                      child: CustomImageWrapper(
                        image: AppIcon.burgerIcon,
                        width: 40,
                        height: 40,
                        fit: BoxFit.contain,
                        isNetworkImage: false,
                      ),
                    ),
                  )
                : Align(
                    alignment: Alignment.centerRight,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.end,
                      padding: const EdgeInsets.only(
                        top: AppGap.large,
                        bottom: AppGap.large,
                      ),
                      children: [
                        //TODO: overflow button
                        ButtonPrimary(
                          'Log in',
                          width: 90,
                          height: 49,
                          buttonColor: AppColors.transparent,
                          elevation: 0,
                          labelColor: AppColors.black,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        ),
                        ButtonPrimary(
                          'Sign Up',
                          width: 90,
                          height: 45,
                          buttonColor: AppColors.primary,
                          elevation: 0,
                          fontSize: AppFontSize.small,
                          labelColor: AppColors.white,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget _topNavigationTablet(BuildContext ctx) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(left: AppGap.giant, right: AppGap.dialog),
      child: IntrinsicHeight(
        child: Stack(
          children: [
            RowPadding(
              padding: EdgeInsets.only(
                top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
                    AppGap.extraLarge,
                bottom: ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                    AppGap.extraLarge,
              ),
              children: [
                const CustomImageWrapper(
                  image: AppIcon.logo,
                  width: 40,
                  height: 40,
                  fit: BoxFit.contain,
                  isNetworkImage: false,
                ),
                const Gap(width: AppGap.normal),
                Text(
                  "Travlog",
                  style: AppTextStyle.black.copyWith(
                    color: AppColors.black,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                        AppFontSize.large,
                        tablet: AppFontSize.medium),
                  ),
                ),
              ],
            ),
            ResponsiveUtils(context).getMediaQueryWidth() > 480 &&
                    ResponsiveUtils(context).getMediaQueryWidth() <= 560
                ? const SizedBox()
                : Align(
                    alignment: Alignment.center,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.center,
                      padding: const EdgeInsets.only(right: AppGap.giant),
                      children: [
                        const SizedBox(width: AppGap.extraDialog),
                        ...List.generate(_topNavList.length, (index) {
                          return TextButton(
                            onPressed: () {
                              _activeIndex = index;
                            },
                            child: Text(
                              _topNavList[index].label,
                              style: AppTextStyle.bold.copyWith(
                                color: _activeIndex == index
                                    ? AppColors.black
                                    : AppColors.grey,
                                fontSize: AppFontSize.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          );
                        }),
                      ],
                    ),
                  ),
            ResponsiveUtils(context).getMediaQueryWidth() > 690
                ? Align(
                    alignment: Alignment.centerRight,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.end,
                      padding: const EdgeInsets.only(
                        top: AppGap.large,
                        bottom: AppGap.large,
                      ),
                      children: [
                        //TODO: overflow button
                        ButtonPrimary(
                          'Log in',
                          width: 90,
                          height: 49,
                          buttonColor: AppColors.transparent,
                          elevation: 0,
                          labelColor: AppColors.black,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        ),
                        ButtonPrimary(
                          'Sign Up',
                          width: 90,
                          height: 45,
                          buttonColor: AppColors.primary,
                          elevation: 0,
                          fontSize: AppFontSize.small,
                          labelColor: AppColors.white,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        )
                      ],
                    ),
                  )
                : const Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(top: 20, right: 24),
                      child: CustomImageWrapper(
                        image: AppIcon.burgerIcon,
                        width: 40,
                        height: 40,
                        fit: BoxFit.contain,
                        isNetworkImage: false,
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget _topNavigationWeb(BuildContext ctx) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(left: AppGap.giant, right: AppGap.dialog),
      child: IntrinsicHeight(
        child: Stack(
          children: [
            RowPadding(
              padding: EdgeInsets.only(
                top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
                    AppGap.extraLarge,
                bottom: ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                    AppGap.extraLarge,
              ),
              children: [
                const CustomImageWrapper(
                  image: AppIcon.logo,
                  width: 40,
                  height: 40,
                  fit: BoxFit.contain,
                  isNetworkImage: false,
                ),
                const Gap(width: AppGap.normal),
                Text(
                  "Travlog",
                  style: AppTextStyle.black.copyWith(
                    color: AppColors.black,
                    fontSize: ResponsiveUtils(context).getResponsiveFontSize(
                        AppFontSize.large,
                        tablet: AppFontSize.medium),
                  ),
                ),
              ],
            ),
            ResponsiveUtils(context).getMediaQueryWidth() < 680
                ? const SizedBox()
                : Align(
                    alignment: Alignment.center,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.center,
                      padding:
                          ResponsiveUtils(context).getMediaQueryWidth() < 775
                              ? const EdgeInsets.only(left: AppGap.extraBig)
                              : const EdgeInsets.only(right: AppGap.large),
                      children: [
                        ...List.generate(_topNavList.length, (index) {
                          return TextButton(
                            onPressed: () {
                              _activeIndex = index;
                            },
                            child: Text(
                              _topNavList[index].label,
                              style: AppTextStyle.bold.copyWith(
                                color: _activeIndex == index
                                    ? AppColors.black
                                    : AppColors.grey,
                                fontSize: AppFontSize.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          );
                        }),
                      ],
                    ),
                  ),
            ResponsiveUtils(context).getMediaQueryWidth() < 775
                ? const Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: AppGap.large,
                          bottom: AppGap.large,
                          right: AppGap.large),
                      child: CustomImageWrapper(
                        image: AppIcon.burgerIcon,
                        width: 40,
                        height: 40,
                        fit: BoxFit.contain,
                        isNetworkImage: false,
                      ),
                    ),
                  )
                : Align(
                    alignment: Alignment.centerRight,
                    child: RowPadding(
                      mainAxisAlignment: MainAxisAlignment.end,
                      padding: const EdgeInsets.only(
                        top: AppGap.large,
                        bottom: AppGap.large,
                      ),
                      children: [
                        //TODO: overflow button
                        ButtonPrimary(
                          'Log in',
                          width: 90,
                          height: 49,
                          buttonColor: AppColors.transparent,
                          elevation: 0,
                          labelColor: AppColors.black,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        ),
                        ButtonPrimary(
                          'Sign Up',
                          width: 90,
                          height: 45,
                          buttonColor: AppColors.primary,
                          elevation: 0,
                          fontSize: AppFontSize.small,
                          labelColor: AppColors.white,
                          borderRadius: AppBorderRadius.ultra,
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
