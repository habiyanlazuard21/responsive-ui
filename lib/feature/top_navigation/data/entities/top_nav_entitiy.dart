class TopNavigationEntity {
  final String label;

  TopNavigationEntity({
    required this.label,
  });

  static List<TopNavigationEntity> topNavigationEntity = [
    TopNavigationEntity(label: "Home"),
    TopNavigationEntity(label: "Discover"),
    TopNavigationEntity(label: "Special Deals"),
    TopNavigationEntity(label: "Contact")
  ];
}
