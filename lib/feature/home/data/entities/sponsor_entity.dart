import '../../../../lib.dart';

class SponsorEntity {
  final String sponsor;

  SponsorEntity(this.sponsor);

  static List<SponsorEntity> getListSponsor = [
    SponsorEntity(AppIllustrations.tripadvisor),
    SponsorEntity(AppIllustrations.expedia),
    SponsorEntity(AppIllustrations.booking),
    SponsorEntity(AppIllustrations.airbnb),
    SponsorEntity(AppIllustrations.rbits),
  ];
}
