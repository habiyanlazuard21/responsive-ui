import 'package:flutter/material.dart';

import '../../../../lib.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<SponsorEntity> _sponsorEntity = SponsorEntity.getListSponsor;

  bool _isMobile = false;

  final String _desc =
      'We always make our customer happy by \nproviding \nas many choices as possible ';

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        if (constraints.maxWidth < 600) {
          return _HomeScreenMobile(
            sponsorEntity: _sponsorEntity,
            desc: _desc,
            isMobile: _isMobile,
          );
        } else {
          return _HomeScreenWeb(
            sponsorEntity: _sponsorEntity,
            desc: _desc,
          );
        }
      },
    );
  }
}

class _HomeScreenMobile extends StatelessWidget {
  const _HomeScreenMobile({
    Key? key,
    required List<SponsorEntity> sponsorEntity,
    String desc = "",
    bool isMobile = false,
  })  : _sponsorEntity = sponsorEntity,
        _desc = desc,
        _isMobile = isMobile,
        super(key: key);

  final List<SponsorEntity> _sponsorEntity;
  final String _desc;
  final bool _isMobile;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      height: 980,
      child: ColumnPadding(
        padding: const EdgeInsets.symmetric(
          horizontal: AppGap.medium,
        ),
        children: [
          const Gap(height: AppGap.big + AppGap.tiny),
          const CustomImageWrapper(
            image: AppImages.travelMobile,
            width: 496,
            height: 386,
            fit: BoxFit.contain,
            isNetworkImage: false,
          ),
          ButtonPrimaryIconRight(
            "Explore the world!",
            isIconDisabled: false,
            labelColor: AppColors.secondary,
            iconColor: AppColors.primary,
            borderRadius: AppBorderRadius.big,
            buttonColor: AppColors.white,
            width: ResponsiveUtils(context).getResponsivePosition(224),
            height: 56,
            onPressed: () {},
          ),
          const Gap(height: AppGap.extraLarge),
          Text.rich(
            TextSpan(
              text: 'Travel',
              style: AppTextStyle.bold.copyWith(
                color: AppColors.black,
                fontSize: AppFontSize.ultra,
              ),
              children: [
                TextSpan(
                  text: ' top \ndestination',
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.secondary,
                    fontSize: AppFontSize.ultra,
                  ),
                ),
                TextSpan(
                  text: ' of the \nworld',
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.ultra,
                  ),
                )
              ],
            ),
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.extraLarge),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.grey,
              fontSize: AppFontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
          const Gap(height: AppGap.extraLarge),
          ButtonPrimary(
            "Get Started",
            buttonColor: AppColors.primary,
            borderRadius: AppBorderRadius.big,
            width: double.infinity,
            height: 65,
            onPressed: () {},
          ),
          const Gap(height: AppGap.extraLarge),
          ButtonPrimaryIconLeft(
            "Watch Demo",
            icon: Icons.play_circle_fill_outlined,
            labelColor: AppColors.black,
            iconColor: AppColors.primary,
            borderRadius: AppBorderRadius.big,
            buttonColor: AppColors.white,
            width: double.infinity,
            height: 72,
            onPressed: () {},
          ),
          const Gap(height: AppGap.extraLarge),
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 60,
            padding: const EdgeInsets.symmetric(
              horizontal: AppGap.medium,
            ),
            child: Wrap(
              spacing: 16,
              runSpacing: 16,
              alignment: WrapAlignment.center,
              children: [
                ...List.generate(
                  _sponsorEntity.length,
                  (index) => CustomImageWrapper(
                    image: _sponsorEntity[index].sponsor,
                    width: 125,
                    height: 22,
                    fit: BoxFit.contain,
                    isNetworkImage: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _HomeScreenWeb extends StatelessWidget {
  const _HomeScreenWeb({
    Key? key,
    String desc = '',
    required List<SponsorEntity> sponsorEntity,
  })  : _getSponsorEntity = sponsorEntity,
        _desc = desc,
        super(key: key);

  final List<SponsorEntity> _getSponsorEntity;
  final String _desc;

  @override
  Widget build(BuildContext context) {
    final isResponsive = ResponsiveUtils(context).getMediaQueryWidth();

    return Container(
      color: AppColors.white,
      width: double.infinity,
      height: ResponsiveUtils(context).getResponsiveSize(
        521,
        tablet: 441,
        desktop: isResponsive > 885 && isResponsive <= 1030
            ? 581
            : isResponsive < 885
                ? 521
                : 721,
      ),
      child: Stack(
        children: [
          Positioned(
            left: 48,
            bottom: ResponsiveUtils(context).getMediaQueryWidth() < 1030
                ? ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                    AppGap.big
                : ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                    AppGap.big,
            child: CustomImageWrapper(
              image: AppIllustrations.backgroundSide,
              width: ResponsiveUtils(context).getResponsiveSize(
                40,
                desktop: isResponsive > 885 && isResponsive <= 1030
                    ? 55
                    : isResponsive < 855
                        ? 40
                        : 65,
              ),
              height: ResponsiveUtils(context).getResponsiveSize(
                129,
                desktop: isResponsive > 885 && isResponsive <= 1030
                    ? 149
                    : isResponsive < 885
                        ? 129
                        : 169,
              ),
              fit: BoxFit.contain,
              isNetworkImage: false,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).viewPadding.left + 128,
              top: ResponsiveUtils(context).getResponsivePadding(
                AppGap.small,
                tablet: AppGap.big,
                desktop: isResponsive > 885 && isResponsive <= 1030
                    ? AppGap.extraBig
                    : isResponsive <= 885
                        ? AppGap.extraBig
                        : 80,
              ),
              right: AppGap.normal,
            ),
            child: Transform.scale(
              alignment: Alignment.topLeft,
              scale: ResponsiveUtils(context).getResponsiveSize(
                0.65,
                tablet: isResponsive <= 720 ? 0.58 : 0.65,
                desktop: isResponsive > 885 && isResponsive <= 1030
                    ? 0.85
                    : isResponsive <= 885
                        ? 0.68
                        : 1,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  ButtonPrimaryIconRight(
                    "Explore the world!",
                    isIconDisabled: false,
                    labelColor: AppColors.secondary,
                    iconColor: AppColors.primary,
                    borderRadius: AppBorderRadius.big,
                    imageSize: AppIconSize.large,
                    buttonColor: AppColors.white,
                    width: 224,
                    onPressed: () {},
                  ),
                  const Gap(height: 23),
                  Text.rich(
                    TextSpan(
                      text: 'Travel',
                      style: AppTextStyle.bold.copyWith(
                        color: AppColors.black,
                        fontSize: AppFontSize.extraUltra,
                      ),
                      children: [
                        TextSpan(
                          text: ' top \ndestination',
                          style: AppTextStyle.bold.copyWith(
                            color: AppColors.secondary,
                            fontSize: AppFontSize.extraUltra,
                          ),
                        ),
                        TextSpan(
                          text: ' \nof the world',
                          style: AppTextStyle.bold.copyWith(
                            color: AppColors.black,
                            fontSize: AppFontSize.extraUltra,
                          ),
                        )
                      ],
                    ),
                    textAlign: TextAlign.left,
                  ),
                  const Gap(height: 23),
                  Text(
                    _desc,
                    style: AppTextStyle.regular.copyWith(
                      color: AppColors.grey,
                      fontSize: AppFontSize.large,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  const Gap(height: 13),
                  Row(
                    children: [
                      ButtonPrimary(
                        "Get Started",
                        buttonColor: AppColors.primary,
                        borderRadius: AppBorderRadius.large,
                        width: 141,
                        height: 49,
                        onPressed: () {},
                      ),
                      const Gap(height: AppGap.dialog),
                      ButtonPrimaryIconLeft(
                        "Watch Demo",
                        icon: Icons.play_circle_fill_outlined,
                        labelColor: AppColors.black,
                        iconColor: AppColors.primary,
                        borderRadius: AppBorderRadius.large,
                        buttonColor: AppColors.white,
                        iconSize: AppIconSize.normal,
                        width: 141,
                        height: 49,
                        onPressed: () {},
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.topRight,
            padding: EdgeInsets.only(
              left: isResponsive < 975
                  ? MediaQuery.of(context).viewPadding.left + 80
                  : MediaQuery.of(context).viewPadding.left + 128,
              right: ResponsiveUtils(context).getResponsiveSize(
                AppGap.small,
                tablet: AppGap.extraSmall,
                desktop: AppGap.normal,
              ),
            ),
            child: CustomImageWrapper(
              image: AppImages.travelWeb,
              width: ResponsiveUtils(context).getResponsiveSize(
                374,
                tablet: isResponsive <= 720 ? 350 : 454,
                desktop: isResponsive > 885 && isResponsive <= 1030
                    ? 574
                    : isResponsive <= 885
                        ? 494
                        : 674,
              ),
              height: ResponsiveUtils(context).getResponsiveSize(
                303,
                tablet: isResponsive <= 720 ? 303 : 383,
                desktop: isResponsive > 885 && isResponsive < 1030
                    ? 513
                    : isResponsive <= 885
                        ? 433
                        : 603,
              ),
              fit: BoxFit.contain,
              isNetworkImage: false,
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.symmetric(
              horizontal: ResponsiveUtils(context).getResponsiveSize(
                AppGap.big,
                tablet: AppGap.extraBig,
                desktop: AppGap.giant,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ...List.generate(_getSponsorEntity.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: AppGap.extraSmall),
                    child: CustomImageWrapper(
                      image: _getSponsorEntity[index].sponsor,
                      width: ResponsiveUtils(context).getResponsiveSize(
                        78,
                        tablet: 88,
                        desktop: isResponsive > 885 && isResponsive <= 1030
                            ? 122
                            : isResponsive < 885
                                ? 98
                                : 148,
                      ),
                      height: ResponsiveUtils(context).getResponsiveSize(
                        24,
                        tablet: 24,
                        desktop: isResponsive > 885 && isResponsive <= 1030
                            ? 30
                            : isResponsive < 885
                                ? 34
                                : 44,
                      ),
                      fit: BoxFit.contain,
                      isNetworkImage: false,
                    ),
                  );
                }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
